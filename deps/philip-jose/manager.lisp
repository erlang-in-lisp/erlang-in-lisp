;;; -*- Mode: Lisp ; Base: 10 ; Syntax: ANSI-Common-Lisp -*-
;;;; Trivial Job Management
(in-package :philip-jose)

#|
Advice from #lisp: maybe use cells or computed-class -- but not really
	http://common-lisp.net/project/cells/
	http://common-lisp.net/project/computed-class/
lemonodor: Jim Firby's RAP (not free software, but a good read)
	http://people.cs.uchicago.edu/~firby/raps/index.shtml

Proper interface:
a little language to dynamically build a dependency graph.
A node remembers those existing nodes that still need to be completed
before that node may be undertaken.
When a node is completed, its dependencies are propagated,
which may themselves generate more jobs...

And the language is Lisp, thanks to CALL/CC!

|#


;;; Global lock to protect state (not used at this time)
#|
(defparameter *tracker-lock* (make-lock)
  "global lock for multithreaded servers")

(defmacro with-tracker-lock (() &body body)
  `(with-lock-held (*tracker-lock*)
     ,@body))
|#

;;; Workers have an id, a known status

(defvar *current-worker*
  nil
  "the worker making current request")

(defparameter *registered-workers*
  (make-hash-table :test 'equal)
  "workers that have registered a job")

(defun register-worker (id &optional (status :working))
  (Setf (gethash id *registered-workers*) status))

(defun unregister-worker (id)
  (remhash id *registered-workers*))


;;; Your every worker job

(defclass worker-job (simple-print-object-mixin)
  ((id :accessor job-id :initarg :id)
   (description :accessor job-description :initarg :description)
   (status :accessor job-status :initarg :status :initform nil) ; nil, t, (worker-id)
   (validator :accessor job-validator :initarg :validator :initform t)
   (on-claim :accessor job-on-claim :initarg :on-claim :initform nil)
   (on-completion :accessor job-on-completion :initarg :on-completion)))

(defparameter *worker-job-counter* 0)

(defun make-worker-job-id ()
  `(,@*id* :worker-job ,(incf *worker-job-counter*)))

(defvar *current-worker-job*
  nil
  "the worker job currently examined")

;;; Worker jobs can be scheduled or claimed

(defparameter *claimed-worker-jobs*
  (make-hash-table :test 'equal)
  "jobs claimed by some worker")

(defun claim-job (job &optional (worker *current-worker*))
  (setf (gethash (job-id job) *claimed-worker-jobs*) job)
  (setf (job-status job) (list worker (get-real-time)))
  (notify-claimed-job job))

(defun notify-claimed-job (job)
  (funkall (job-on-claim job)))

(defun notify-completed-job (job &rest results)
  (setf (job-status job) :completed)
  (apply #'funkall (job-on-completion job) results))

(defun unclaim-job (job)
  (setf (job-status job) '(:unclaimed))
  (remhash (job-id job) *claimed-worker-jobs*)
  nil)

(defparameter *scheduled-worker-jobs*
  (make-fifo)
  "jobs that need to be executed by some worker")

(defun format-job (stream job)
  "Format only the interesting slots of job to stream."
  (format stream "#<job ~d ~A>"
          (job-id job)
          (job-description job)))

(defun enqueue-worker-job (job)
  (logger "~&Enqueueing ~A" (format-job nil job))
  (fifo-enqueue job *scheduled-worker-jobs*)
  job)

(defun issue-worker-job (description &rest keys
                        &key validator on-claim on-completion)
  (declare (ignore validator on-claim on-completion))
  (enqueue-worker-job
   (apply #'make-instance 'worker-job
          :id (make-worker-job-id)
          :description description
          keys)))

(defun get-next-worker-job ()
  (loop until (fifo-empty-p *scheduled-worker-jobs*)
        do (let ((job (fifo-dequeue *scheduled-worker-jobs*)))
             (when (validate-job job)
               (logger "~&Issuing ~A" (job-id job))
               (return job)))))

;;; Global defaults when looking for a job

(defun get-next-worker-job-specification ()
  (if-bind job (get-next-worker-job)
    (progn
      (claim-job job)
      (job-specification job))
    (no-job-to-do)))

(defun job-specification (job)
  `(:job :id ,(job-id job) :description ,(job-description job)))

(defparameter *all-done*
  nil
  "when everything is said and done, we'll tell each worker to die in peace")

(defun no-job-to-do ()
  (if *all-done*
      (suicide-job)
      (sleep-job)))

(defun suicide-job (&optional (worker *current-worker*))
  (unregister-worker worker)
  '(:die))

(defun sleep-job ()
  `(:sleep ,*sleep-delay*))

(defgeneric validate-job (job)
  (:method ((x t))
    t))

(defmethod validate-job ((job worker-job))
  (funkall (or (job-validator job) t)))

(defun hand-job-over-to-worker (job &optional (worker *current-worker*))
  (with-accessors ((id job-id) (desc job-description)
                   (on-claim job-on-claim) (on-completion job-on-completion))
      job
    (register-worker worker id)
    (claim-job id worker)))

(defun-request-handler worker-request (id &rest keys &key completed results error)
  (logger "~&Got worker-request from~{ ~A~}, with keys ~S" id keys)
  (when error
    (logger "~&Error from worker~{ ~A~}: ~A" id error))
  (let ((*current-worker* id))
    (when completed
      (job-done completed id results))
    (reply (get-next-worker-job-specification))))

(defun job-done (job-id worker results)
  (if-bind job (gethash job-id *claimed-worker-jobs*)
    (destructuring-bind (claimant start-time) (job-status job)
      (if (equal worker claimant)
          (logger "~&Job ~D completed by worker ~S in ~D seconds"
                  (job-id job) worker
                  (- (get-real-time) start-time))
          (logger "~&Worker ~S announced he completed job ~A, originally claimed by ~S"
                  worker (format-job nil job) claimant))
      (unclaim-job job)
      (register-worker worker :idle)
      (apply #'notify-completed-job job results))
    (logger "~&Worker ~S announced completion of job ~A, that doesn't exist (anymore?)"
            worker (format-job nil job))))

(defun-request-handler show-job-status ()
  (reply* :all-done *all-done* :claimed-worker-jobs (hash-table->alist *claimed-worker-jobs*)))

(defun notify-worker-timeout (worker &aux (pity nil))
  ;; In this Trotskyist/Guevarist approach to management, we simply kill those
  ;; counter-revolutionary saboteurs who are late at delivering the results
  ;; expected by the manager. Oh, of course, being good socialists, we never
  ;; kill any worker. Mind you, the miserable insect is unregistered first, so
  ;; that not being a worker anymore, it is killed without spilling sacred
  ;; proletarian blood. Insects are worthless, and multiply anyway.
  (unregister-worker worker)
  (destructuring-bind (insect without rights) worker
    (declare (ignore rights))
    (kill-machine-process insect without) pity
    (spawn-client insect)))


;;; Advanced Control Flow

(exporting-definitions

(defvar *retry-on-timeout* nil)

(defun/cc issue-worker-job/cc (description &key validator on-claim on-completion
                                           retry-on-timeout)
  (if retry-on-timeout
      (loop
        for attempts from 1 do
        (let ((guard t) (worker nil))
          (multiple-value-bind (successp &optional results)
              (with-local-task-competition (c)
                (issue-worker-job
                   description
                   :validator (lambda ()
                                (and guard (funkall (or validator t))))
                   :on-claim (lambda ()
                               (funkall
                                (prog1 on-claim
                                  (setf on-claim nil
                                        worker *current-worker*)
                                  (local-task-competitor (c) (sleep/cc retry-on-timeout) nil))))
                   :on-completion (lambda (&rest results)
                                    (setf worker nil)
                                    (maybe-win-local-task-competition c t results))))
            (DBG :gah guard successp results attempts)
            (setf guard nil)
            (if successp
                (return (apply on-completion results))
                (when worker
                  (logger "~&Timed out:~{ ~A~}" worker)
                  (notify-worker-timeout worker))))))
      (issue-worker-job description
                        :validator validator
                        :on-claim on-claim
                        :on-completion on-completion)))

(defun/cc issue-sequential-job (description &key retry-on-timeout)
  (let/cc k
    (issue-worker-job/cc
     description
     :retry-on-timeout retry-on-timeout
     :on-completion k)))

(defun/cc issue-sequential-job-with-timeout
    (description &key timeout retry-on-timeout validator on-claim on-completion)
  (let ((guard t) worker)
    (multiple-value-bind (successp &optional results)
        (with-local-timeout (timeout competition)
          (let/cc k
            (issue-worker-job/cc
             description
             :retry-on-timeout retry-on-timeout
             :validator (lambda () (and guard (funkall (or validator t))))
             :on-claim (lambda () (setf worker *current-worker*) (funkall on-claim))
             :on-completion (lambda (&rest results)
                              (setf worker nil)
                              (funcall on-completion)
                              (kall k t results)))))
      (setf guard nil)
      (when worker
        (notify-worker-timeout worker))
      (apply #'values results))))

(defun/cc call-with-parallel-job-issuer (thunk)
  ;;TODO: add reduce functionality on results
  (let/cc k
    (let ((remaining-jobs 0))
      (labels ((maybe-exit ()
                 (when (zerop remaining-jobs)
                   (kall k)))
               (one-less ()
                 (decf remaining-jobs)
                 (maybe-exit))
               (one-more (description &key retry-on-timeout on-claim on-completion)
                 (let/cc k
                   (incf remaining-jobs)
                   (issue-worker-job/cc
                    description
                    :retry-on-timeout retry-on-timeout
                    :on-claim (lambda ()
                                (funkall on-claim)
                                (kall k))
                    :on-completion (lambda (&rest x)
                                     (apply #'funkall on-completion x)
                                     (one-less))))))
        (funcall thunk #'one-more)
        (maybe-exit)))))

(defparameter *parallel-job-issuer* nil)

(defun/cc issue-parallel-job (description &key retry-on-timeout (issuer *parallel-job-issuer*))
  (unless issuer
    (error "No parallel job issuer for job ~S" description))
  (funcall issuer description :retry-on-timeout retry-on-timeout))

(defmacro with-parallel-jobs ((&optional issuer fun) &body body)
  (with-gensyms (args)
    `(call-with-parallel-job-issuer
      (lambda (,(or issuer '*parallel-job-issuer*))
        (labels (,(when (or issuer fun)
                    `(,(or fun issuer) (&rest ,args)
                       (apply ,(or issuer '*parallel-job-issuer*) ,args))))
          ,@body)))))

)
