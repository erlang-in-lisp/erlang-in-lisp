;;;-*- Lisp -*-

(in-package :cl-user)

(proclaim '(optimize (speed 3) (safety 2) (debug 1) (space 3)))

(asdf:oos 'asdf:load-op :cl-launch)

(asdf:defsystem :philip-jose
  :name "Philip Jose Farmer"
  :serial nil
  :depends-on (:cl-launch ;; needed first, for fasl caching(?)
               :cffi :iolib :io.multiplex ;;:net.dns-client
               :split-sequence :parse-number ;:net-telent-date ;;removed because I can't get it
               :bordeaux-threads :arnesi :closer-mop
               :fare-utils)
  :components ((:file "package")
	       (:file "utilities" :depends-on ("package"))
	       (:file "specials" :depends-on ("package"))
	       (:file "logger" :depends-on ("specials"))
	       (:file "registry" :depends-on ("logger" "utilities"))
	       (:file "trivial-sexp-server" :depends-on ("logger"))
	       (:file "incremental-parsing" :depends-on ("trivial-sexp-server"))
	       (:file "tcp-client" :depends-on ("logger"))
	       (:file "worker" :depends-on ("tcp-client" "utilities" "specials"))
	       (:file "machines" :depends-on ("trivial-command-line" "registry" "worker" "specials"))
               (:file "trivial-command-line" :depends-on ("logger"))
	       (:file "tcp-server" :depends-on ("trivial-sexp-server" "incremental-parsing"
                                                                      "logger"))
	       (:file "local-tasks" :depends-on ("logger" "utilities"))
	       (:file "manager" :depends-on
                      ("tcp-server" "tcp-client" "logger" "utilities" "local-tasks"))
	       (:file "farmer" :depends-on ("machines" "registry" "manager"))))
