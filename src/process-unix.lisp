;;;-*- Lisp -*-

(in-package #:erlang-in-lisp)

;;Here are all the specializations of the methods in
;;process.lisp that implement the unix-process based
;;forking model.

(defclass unix-process (process-with-mailbox)
  ((unix-pid :accessor process-unix-pid
	     :initarg :unix-pid)
   (parent-pid :accessor process-parent-pid
	       :initarg :parent-pid
	       :initform -1)
   (child-list :accessor process-child-list
	       :initform (list))
   (hostname :accessor process-hostname 
	     :initarg :hostname)
   (receive-socket :accessor process-receive-socket
		   :initarg :receive-socket)))

(defclass remote-unix-process (process) 
  ((hostname :accessor remote-unix-process-hostname 
	     :initarg :hostname)
   (port :accessor remote-unix-process-port 
	 :initarg :port)
   (unix-pid :accessor remote-unix-process-pid 
	     :initarg :unix-pid)))

(defmethod pop-pending ((current unix-process) timeout)
  (let ((msg (call-next-method)))
    (if (not (null msg))
	msg
	(progn ;;not ideal...should push all pending messages
	  (iolib:with-accept-connection (connection (process-receive-socket current) :timeout timeout)
	    (when (null connection) ;;timeout, no connection
	      (error 'message-timeout))
	    ;;(cl-store:restore connection))))))
	    (read connection))))))
  
(defun to-remote-unix-process (unix-process)
  (with-slots (name receive-socket unix-pid hostname) unix-process
    (make-instance 'remote-unix-process
		   :name name
		   :port (iolib:local-port receive-socket)
		   ;;the address in vector form...this isn't right:
		   :hostname (net.sockets::address-name hostname) 
		   :unix-pid unix-pid)))


;;still need to fix some problems with (self) in 
;;unix-process based concurrency...what if we do a (self) then fork
;;with that binding in the new process...will not work.

(defmethod print-object ((remote-process remote-unix-process) s)
  (with-slots (name hostname port unix-pid) remote-process
    ;;this is truly aweful:
    (format s
	    "#.(MAKE-INSTANCE 'erlang-in-lisp::remote-unix-process :NAME '~S :HOSTNAME ~S :PORT ~S :UNIX-PID ~S)"
	    name hostname port unix-pid)))

(defmethod print-object ((process unix-process) s)
  (print-object (to-remote-unix-process process) s))

(defun add-child-process (current-process child-process-descriptor)
  (push child-process-descriptor 
	(process-child-list current-process))
  child-process-descriptor)

(defun remove-child-process (current-unix-process unix-pid)
  "Given a unix process id, remove the corresponding pid from *child-process-list*."
  (setf (process-child-list current-unix-process)
	(remove-if #'(lambda (pid) (eql (remote-unix-process-pid pid) unix-pid)) 
		   (process-child-list current-unix-process))))

(defmethod spawn* ((current unix-process) (name symbol) (func function))
  ;;the call to create the child socket, must occur before the fork...
  (let* ((child-socket 
	  (iolib:make-socket :ipv6 nil 
			     :connect :passive 
			     :local-host (process-hostname current)))
	 (child-process 
	  (make-instance 'unix-process 
			 :name name
			 :hostname (process-hostname current)
			 :parent-pid (getpid)
			 :receive-socket child-socket))
	 (child-unix-pid 
	  (do-fork)))
    (if (zerop child-unix-pid) 
	(progn 
	  ;;inside child process
	  (setf *current-process* child-process)
	  (setf (process-unix-pid *current-process*) (getpid))
	  (call-next-method) ;;parent class has func call and error handling code
	  (do () ((null (process-child-list *current-process*)) 'done) 
	    (let ((wait-for-pid (wait))) ;;make sure we only wait for the exit status
	      (remove-child-process *current-process* wait-for-pid)))
	  (close (process-receive-socket *current-process*))
	  (quit))
	;;inside parent process
	(progn
	  (setf (process-unix-pid child-process) child-unix-pid)
	  (add-child-process current (to-remote-unix-process child-process))))))

(defmethod send* ((current unix-process) (to remote-unix-process) &rest msg)
  ;;(format t "going to connect to: ~A:~A~%" (remote-unix-process-hostname to) (remote-unix-process-port to))
  (iolib:with-open-socket (socket :ipv6 nil) ;;cache these connections?
    (iolib:connect socket 
		   (iolib:make-address (remote-unix-process-hostname to))
		   :port (remote-unix-process-port to))
    ;;(cl-store:store msg socket)
    (print msg socket)
    (finish-output socket)))

;;(defmethod send* ((current unix-process) (to unix-process) &rest msg)
;;"Sending a message to ourself.")

(defmethod toplevel* ((current unix-process) local-address)
  (setf 
   (process-unix-pid current) (getpid)
   (process-hostname current) local-address
   (process-receive-socket current) (iolib:make-socket :ipv6 nil
						       :connect :passive
						       :local-host local-address)
   (process-eil-pmd current) (spawn 'eil-pmd #'eil-pmd))
  current)

(defmethod toplevel-exit* ((current unix-process))
  (send (process-eil-pmd current) :done)
  (do () ((null (process-child-list current)) 'done) 
    (let ((wait-for-pid (wait))) ;;make sure we only wait for the exit status
      (remove-child-process current wait-for-pid)))
  (close (process-receive-socket current)))

(defmethod process-equal-p and ((p1 remote-unix-process) (p2 remote-unix-process))
  (and ;;(equal (remote-unix-process-hostname p1) (remote-unix-process-hostname p2))
       (equal (remote-unix-process-port p1) (remote-unix-process-port p2))
       (equal (remote-unix-process-pid p1) (remote-unix-process-pid p2))))

(defmethod process-equal-p :around ((p1 unix-process) (p2 unix-process))
  (process-equal-p (to-remote-unix-process p1) (to-remote-unix-process p2)))

(defmethod process-equal-p :around ((p1 remote-unix-process) (p2 unix-process))
  (process-equal-p p1 (to-remote-unix-process p2)))

(defmethod process-equal-p :around ((p1 unix-process) (p2 remote-unix-process))
  (process-equal-p (to-remote-unix-process p1) p2))

(fiveam:in-suite erlang-in-lisp::eil-suite)

(fiveam:test add-remove-child-process-test
 "Test the creation of processes and adding/removing child process descriptors." 
 (let* ((p1 (make-instance 'unix-process :name 'p1 :unix-pid 1))
	(unix-id1 1234)
	(unix-id2 5678)
	(pd1 (make-instance 'remote-unix-process :name 'pd1 :unix-pid unix-id1))
	(pd2 (make-instance 'remote-unix-process :name 'pd2 :unix-pid unix-id2)))   
   (is (null (process-child-list p1)))

   (add-child-process p1 pd1)
   (add-child-process p1 pd2)
   (is (eql 2 (length (process-child-list p1))))
   (is (member pd1 (process-child-list p1)))
   (is (member pd2 (process-child-list p1)))

   (remove-child-process p1 unix-id1)
   (is (eql 1 (length (process-child-list p1))))
   (is (not (member pd1 (process-child-list p1))))
   (is (member pd2 (process-child-list p1)))))

(fiveam:test unix-process-equal-p-test
  "Test process-equal-p for unix based processes."
  (let ((p1 (make-instance 'remote-unix-process 
			   :name 'p1
			   :hostname nil
			   :port 1234
			   :unix-pid 1))
	(p2 (make-instance 'remote-unix-process 
			   :name 'p1
			   :hostname nil
			   :port 1234
			   :unix-pid 1))
	(p3 (make-instance 'remote-unix-process 
			   :name 'p3
			   :hostname nil
			   :port 1234
			   :unix-pid 1)))
    (is (process-equal-p p1 p2))
    (is (not (process-equal-p p1 p3)))))

