;;;-*- Lisp -*-
(in-package #:erlang-in-lisp)

;;Here we have the processes with thread based concurrency using
;;bordeaux threads.  This is really more of proof of concept to show
;;whether or not the new CLOS based abstraction is useful

(defclass thread-process (process-with-mailbox)
  ((mailbox-lock :accessor mailbox-lock
		 :initform (bordeaux-threads:make-lock))
   (mailbox-condition :accessor mailbox-condition
		      :initform (bordeaux-threads:make-condition-variable))
   (thread :accessor process-thread))
  (:documentation "Process class for thread based concurrency.  It
only has one lock (this should probably be changed at some point) that
guards access to the mailbox."))

(defmethod spawn* ((current thread-process) (name symbol) (func function))
  (let ((new-process (make-instance 'thread-process :name name)))
    (setf (process-thread new-process) 
	  (bordeaux-threads:make-thread 
	   #'(lambda () 
	       (let ((*current-process* new-process)
		     (cl-user::*standard-output* cl-user::*stdout*));;for debugging
		 ;;(format t "in lambda cpid: ~A~%" *current-process*)
		 (call-next-method)))
	   :name (symbol-name name)))
    new-process))

(defmethod send* ((current thread-process) (to thread-process) &rest msg)
  (push-pending to msg))

;;we override the mailbox methods, wrapping each access of the mailbox
;;in the a lock.  A simple improvement would be two locks (one for the
;;pending list and another for the checked list).  A more
;;sophisticated system might use STM, but this is really a proof a
;;concept.
(defmethod pop-pending ((process thread-process) timeout)
  (bordeaux-threads::with-lock-held ((mailbox-lock process))
    (with-slots (mailbox-lock mailbox-condition) process
      (when (pending-messages-empty-p process) ;;change to loop to handle premature awakenings?
	(bordeaux-threads:condition-wait mailbox-condition mailbox-lock))
      (call-next-method))))

(defmethod push-pending ((process thread-process) msg)
  (bordeaux-threads:with-lock-held ((mailbox-lock process))
    (call-next-method)
    (bordeaux-threads:condition-notify (mailbox-condition process))))

(defmethod push-checked ((process thread-process) msg)
  (bordeaux-threads:with-lock-held ((mailbox-lock process))
    (call-next-method)))

(defmethod merge-pending-checked ((process thread-process))
  (bordeaux-threads:with-lock-held ((mailbox-lock process))
    (call-next-method)))



(defmethod toplevel* ((current thread-process) local-address)
  (setf (process-eil-pmd current) (spawn 'eil-pmd #'eil-pmd)))

;;join with child threads or something like that?
(defmethod toplevel-exit* ((current thread-process))
  (send (process-eil-pmd current) :done))
  
(defmethod process-equal-p :around ((p1 thread-process) (p2 thread-process))
  (eq p1 p2)) ;;should be identical not just logically equivalent

(fiveam:in-suite erlang-in-lisp::eil-suite)

(fiveam:test thread-process-send-test
  "Make sure the send method for threads places an item in the pending-list"
  (toplevel :process-class 'thread-process)
  (with-slots (pending-list checked-list) (self)

    (is (empty-queue-p pending-list))
    
    (send (self) :hello 1 2 3)
    
    (is (equal (front pending-list) (list :hello 1 2 3))))
  (toplevel-exit))

