;;; -*- Mode: Lisp ; Base: 10 ; Syntax: ANSI-Common-Lisp -*-
;;;;; Forking

(in-package #:erlang-in-lisp)


(defun do-fork ()
  #+clisp (unix:fork)
  #+sbcl (sb-posix:fork))

(defun dup2 (oldfd newfd) 
  #+clisp (unix:dup2 oldfd newfd)
  #+sbcl (sb-posix:dup2 oldfd newfd))

(defun fchdir (fd) 
  #+clisp (unix:fchdir fd)
  #+sbcl (sb-posix:fchdir fd))

(defun setuid (uid) 
  #+clisp (unix:setuid uid)
  #+sbcl (sb-posix:setuid uid))

(defun setgid (gid) 
  #+clisp (unix:setgid gid)
  #+sbcl (sb-posix:setgid gid))

(defun quit () 
  #+clisp (ext:quit)
  #+sbcl (sb-ext:quit))

(defun wait (&optional statusptr) 
  #+clisp (unix:wait)
  #+sbcl (sb-posix:wait statusptr))

(defun getpid () 
  #+clisp (unix:getpid)
  #+sbcl (sb-posix:getpid))

;;waitpid


;;ok, I found these abstractions to be a bit less useful than initially thought.
;;I'll just keep the functions in this file as light-weight as possible for now
;;and do all the real work in process.lisp

#|(defun fork (&rest func-and-args)
"Fork.  If a function is specified as the first argument, call it in the child process.
Additional arguments are passed to the function call"
  (let ((pid (do-fork)))
    (when (zerop pid)
      ;;inside the child process
      (when func-and-args ;;add in check to make sure first args if function?
	(apply (first func-and-args)  (rest func-and-args))))
    pid))



(defun fork-call-and-exit (&rest func-and-args)
"Fork, call the specified function in the child process, then exit."
  (let ((pid (apply #'fork func-and-args)))
    (when (zerop pid)
      (quit))
    pid)) ;;need an explicit wait in here to avoid a zombie processes
|#






