(in-package #:erlang-in-lisp)

;;This file contains the generic methods that are the heart of the
;;system  They are not meant to me called by users of erlang-in-lisp
;;directly (instead a thin macro layer provides the outward face of
;;erlang-in-lisp), but these generic methods _are_ meant to be
;;overriden to create new concurrency strategies, speedier mailbox
;;implementations, etc.  

(defparameter *current-process* nil)

(defclass process ()
  ((name :accessor process-name
	 :initarg :name)
   (eil-pmd :accessor process-eil-pmd)
   (linked-pids :accessor process-linked-pids :initform nil)))

(defclass process-mailbox ()
  ((pending-list :initform (make-queue))
   (checked-list :initform (make-queue))))

(define-condition message-timeout () ())

(defgeneric pop-pending (mailbox timeout)
(:documentation "Return the first pending message.  If there are no
messages, block until one is received or signal a message-timeout
error if the amount of time specified in the timeout parameter has
passed."))
(defmethod pop-pending ((mailbox process-mailbox) timeout)
  (with-slots (pending-list) mailbox
    (let ((msg (front pending-list)))
      (dequeue pending-list)
      msg)))

(defgeneric push-pending (mailbox msg)
(:documentation "Push a pending message into the specified mailbox.
The message is new and has not been checked for a match. In a threads
based implementation, for example, this method would be called on the
receiving thread by the sending thread."))
(defmethod push-pending ((mailbox process-mailbox) msg)
  (with-slots (pending-list) mailbox
    (enqueue msg pending-list)))

(defgeneric push-checked (mailbox msg)
(:documentation "Push a message that has been checked for a match but
has failed (it does not match the pattern) into the checked list."))
(defmethod push-checked ((mailbox process-mailbox) msg)
  (with-slots (checked-list) mailbox
    (enqueue msg checked-list)))

(defgeneric merge-pending-checked (mailbox) 
(:documentation "Merge the pending and checked lists.  All messages
are then in the pending list and the checked list is empty."))
(defmethod merge-pending-checked ((mailbox process-mailbox))
  (with-slots (pending-list checked-list) mailbox
    ;;Slow but works?
    (mapcar  #'(lambda (x) (enqueue x pending-list)) (queue-contents checked-list))
    ;;Something is wrong with this destructive implementation:
    ;;(queue-nconc pending-list (queue-contents checked-list))
    (setf checked-list (make-queue))))

(defgeneric pending-messages-empty-p (mailbox)
(:documentation "t if there are no pending messages, otherwise nil."))
(defmethod pending-messages-empty-p ((mailbox process-mailbox))
  (with-slots (pending-list) mailbox
    (empty-queue-p pending-list)))

(defgeneric checked-messages-empty-p (mailbox)
(:documentation "t if there are no checked messages, otherwise nil."))
(defmethod checked-messages-empty-p ((mailbox process-mailbox))
  (with-slots (checked-list) mailbox
    (empty-queue-p checked-list)))


(defclass process-with-mailbox (process process-mailbox) ())

(defgeneric self* (process)
  (:documentation "Return the pid, the process descriptor, of the
current process.  For now, this function should be used in lieu of
accessing the special *current-process* variable directly in case the
mechanism for locating this pid changes in the future."))
(defmethod self* ((process process)) *current-process*)


(defgeneric send* (from-process to-process &rest msg)
  (:documentation "Send a message from the specified process to the
specified process."))


(defgeneric receive* (process receive-func timeout after-func))
(defmethod receive* ((process process-with-mailbox)
		     (receive-func function)
		     timeout
		     (after-func function))
  (handler-case
      (let* ((msg (pop-pending process timeout))
	     (result (funcall receive-func msg)))
	(if (null result)
	    (progn
	      ;;(format t "no match: ~A~%" msg)
	      (push-checked process msg)
	      (receive* process receive-func timeout after-func))
	    (progn
	      ;;(format t "match: ~A~%" msg)
	      (merge-pending-checked process)
	      (funcall result))))
    (message-timeout (ct)
      (identity ct) ;;I'm just annoyed with the compiler messages.
      (merge-pending-checked process)
      (funcall after-func))))


;;the generic methods used to implement spawn() behavior
(defgeneric spawn* (parent-process name func)
  (:documentation "Spawn a new process."))

(defmethod spawn* ((parent-process process) (name symbol) (func function))
  ;;for now all processes handle errors like erlang 'system processes'
  (handler-case
      (funcall func)
    (condition (cnd) ;;catch all the unhandled conditions
      (let ((condition-msg (condition-to-message cnd))) ;;convert to msgs
	(dolist (linked-process (process-linked-pids (self))) ;;send to linked processes
	  (send linked-process condition-msg))))))

(defgeneric toplevel* (process local-adress))

(defgeneric toplevel-exit* (process))

(defgeneric condition-to-message (condition)
(:documentation "Convert a condition to a message that can be sent and
received.  In some implementations (i.e. for a shared memory threads
implementation) it might make the most sense to simply return the
original condition."))
(defmethod condition-to-message ((condition condition))
  (list 'error (class-name (class-of condition))))

(defgeneric link* (process pid))
(defmethod link* ((current process) (pid process))
  (push (process-linked-pids current) pid))

(defgeneric unlink* (process pid))
(defmethod unlink* ((current process) (pid process))
  (with-slots (linked-pids) current
    (setf linked-pids (remove pid linked-pids :test #'equal))))

(defgeneric process-equal-p (p1 p2)
(:documentation "Utility method, primarily for testing, to determine
whether two processes are logically equivalent.")
(:method-combination and))
(defmethod process-equal-p and ((p1 process) (p2 process))
  (equal (process-name p1) (process-name p2)))

		  
#| If we really want to compile Erlang to EIL,
I think Robert's suggestion is a good one:
We really should focus on compiling core erlang

These are just yanked from the core erlang spec:
(defun eil-after ())
(defun eil-apply ())
(defun eil-attributes ())
(defun eil-call ())
(defun eil-case ())
(defun eil-catch ())
(defun eil-do ())
(defun eil-end ())
(defun eil-fun ())
(defun eil-in ())
(defun eil-let ())
(defun eil-letrec ())
(defun eil-module ())
(defun eil-of ())
(defun eil-primop ())
(defun eil-receive ())
(defun eil-try ())
(defun eil-when ())
|#

(fiveam:in-suite erlang-in-lisp::eil-suite)

(fiveam:test push-pop-pending-test
  (let ((p (make-instance 'process-with-mailbox)))
    (push-pending p 1)
    (push-pending p 2)
    (push-pending p 3)
    (is (eql (pop-pending p nil) 1))
    (is (eql (pop-pending p nil) 2))
    (is (eql (pop-pending p nil) 3))
    (is (pending-messages-empty-p p))
    (is (checked-messages-empty-p p))))

(fiveam:test checked-merge-test
  (let ((p (make-instance 'process-with-mailbox)))
    (push-pending p 1)
    (push-checked p 2)
    (push-checked p 3)
    (merge-pending-checked p)
    (is (eql (pop-pending p nil) 1))
    (is (eql (pop-pending p nil) 2))
    (is (eql (pop-pending p nil) 3))
    (is (pending-messages-empty-p p))
    (is (checked-messages-empty-p p))))

    
