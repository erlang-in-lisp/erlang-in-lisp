;;Misc stuff goes here for now until I put it in 
;;a real unit testing framework (if it belongs there)
(in-package #:erlang-in-lisp)

;;my crap
(defun fib (n)
  (if (<= n 2)
      1
      (+ (fib (- n 1)) (fib (- n 2)))))

(defun fork-test()
  (let ((pid (fork 
	      (format t "~%child start")
	      (fib 40)
	      (format t "~%child exit")
	      (sb-ext:quit))))
    (format t "~%parent start")
    (fib 40)
    (sb-posix:waitpid pid 0)
    (format t "~%parent exit")))

;;iolib crap


(defun asdf ()
  (iolib:with-open-socket (s :ipv6 nil :connect :passive)
    ;;need a listen in here?
    ;;(iolib:listen-on s)
    (iolib:with-accept-connection (c s)
      (print c))))


(defparameter *soc* (iolib:make-socket :ipv6 nil))
(iolib:connect *soc* (iolib:make-address #(127 0 0 1)) :port 8887)

;;make the passive server socket, still need to 
;;do accepts and whatnot on this
(defun make-tcp-server-socket (address port)
  (let ((socket (iolib:make-socket :address-family :internet
				   :type :stream
				   :connect :passive
				   :ipv6 nil)))
    (iolib:bind-address socket address :port (or port 0))
    (iolib:listen-on socket)
    socket))





