(in-package #:erlang-in-lisp)
;;erlang-in-lisp process manager daemon

#|
The problem is delivering messages.  How do we implement a concurrent 
message passing system:
a) inside of one address space
b) with multiple address spaces (files, sockets)

The first step is to figure out how messages are delivered in erlang
the current erlang implementation.  Each process has its own heap
space, but are the messages delivered on this heap or elsewhere?
Since each erlang-process is inside of one unix-process, it is
conceivable that under the hood there is a mapping from pid->mailbox
and that the mailboxes are accessed and mutated through a traditional
concurrency mechanism like locks.

In Erlang, message delivery is a guarantee and thus does not or cannot
fail.  Perhaps recovering from a message delivery failure is beyond
the scope of what we want to handle.  Sure, this should not happen,
and messages should always be delivered, but we should not push these
concerns into user code.

For multiple address spaces we need to know where the messages go, we
can:
a) always hand the messages off to some other process that stays the
   same knows how to properly deliver the message (i.e. a running
   daemon on a particular socket)

b) have all the information needed to deliver the message embedded in
   the pid

For now have multiple messaging techniques that are handled by something 
like CLOS:
a) every process with its own socket
b) every message to a daemon
c) shared memory mailboxes
|#

(defconstant eil-pmd-port-number 8889)
(defparameter *ptable* (make-hash-table))

;;as Fare has mentioned, this is one case where we definitely need
;;to consider a kqueue/epoll style event loop (using iolib):
(defun eil-pmd ()
  "The erlang-in-lisp process manager daemon."
  (loop
       (receive 
	 ((list :register from name pid)
	  (send from 
	    (self) (register-process name pid)))

	 ((list :unregister from name)
	  (send from 
	    (self) (unregister-process name)))

	 ((list :whereis from name)
	  (send from 
	    (self) (lookup-process name)))
	 
	 ((list :done)
	  (return-from eil-pmd)))))

(defun register-process (name pid)
  (setf (gethash name *ptable*) pid))

(defun unregister-process (name)
  (remhash name *ptable*))

(defun lookup-process (name)
  (gethash name *ptable*))

;;unit tests for this file
(fiveam:in-suite erlang-in-lisp::eil-suite)

(fiveam:test ptable-test
  "Test the process registration, lookup, unregistration." 
  (let ((*ptable* (make-hash-table))
	(pid1 (make-instance 'unix-process :name 'p1 :unix-pid 1))
	(pid2 (make-instance 'unix-process :name 'p2 :unix-pid 2))
	(pid3 (make-instance 'unix-process :name 'p3 :unix-pid 3))
	(pid1-name 'pid1)
	(pid2-name 'pid2)
	(pid3-name 'pid3))
    (declare (ignore pid3))

    ;;make sure the table is empty
    (is (null (lookup-process pid1-name)))
    (is (null (lookup-process pid2-name)))
    (is (null (lookup-process pid3-name)))

    ;;register two, make sure they're there
    (register-process pid1-name pid1)
    (register-process pid2-name pid2)
    (is (equal pid1 (lookup-process pid1-name)))
    (is (equal pid2 (lookup-process pid2-name)))
    (is (null (lookup-process pid3-name)))

    ;;remove one, check
    (unregister-process pid1-name)
    (is (null (lookup-process pid1-name)))
    (is (eql pid2 (lookup-process pid2-name)))
    (is (null (lookup-process pid3-name)))))


