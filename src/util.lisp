(in-package #:erlang-in-lisp)

;;from Norvig pg 342
(proclaim '(inline queue-contents make-queue enqueue dequeue 
	    front empty-queue-p queue-nconc))

(defun queue-contents (q) (cdr q))

(defun make-queue ()
  (let ((q (cons nil nil)))
    (setf (car q) q)))

(defun enqueue (item q)
  (setf (car q)
	(setf (rest (car q))
	      (cons item nil))))

(defun dequeue (q)
  (pop (cdr q))
  (if (null (cdr q)) (setf (car q) q))
  q)

(defun front (q) (first (queue-contents q)))

(defun queue-nconc (q list)
  (when (and (not (null list)) (not (empty-queue-p q)))
    (setf (car q)
	  (last (setf (rest (car q)) list)))))

(defun empty-queue-p (q) (null (queue-contents q)))


;;destructive removes
;;(defun remove* (list item &rest args)
;;  (apply #'remove item list args))

;;(define-modify-macro remove! (list item &rest args)
;;  ;;(item &key from-end test test-not start end count key)
;;  remove*
;;  "Remove an item from a list")