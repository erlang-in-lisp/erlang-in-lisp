(in-package #:erlang-in-lisp)

;;This file contains higher level abstractions that are built on top
;;of send/receive.  See core-eil.lisp for the low level macro layer.

;;simple rpc
(defun rpc-send (pid timeout &rest msg)
  "Synchronous send.  Send and block for a response."
  (apply #'send pid msg)
  (receive-after 
   (((list pid response) response))
   timeout 
   ((error 'rpc-timeout))))

;;registration
(defun register (name pid)
  "Maps a particular process to a particular name."
  (rpc-send (process-eil-pmd *current-process*) 2 :register (self) name pid))

(defun unregister (name)
  "Destroy the mapping specified by name."
  (rpc-send (process-eil-pmd (self)) 2 :unregister (self) name))

(defun whereis (name)
  "Returns the process mapped to the specified name."
  (rpc-send (process-eil-pmd (self)) 2 :whereis (self) name))

(defun registered ()
  "Returns a list of all registered processes."
  (rpc-send (process-eil-pmd (self)) 2 :registered (self)))


;;error handling
(defun spawn-link (name func)
  (let ((pid (spawn name func)))
    (link pid)
    pid))

(defun process-flag ()
  (error "Not yet implemented."))

(defun link (pid)
  (link* (self) pid))

(defun unlink (pid)
  (unlink* (self) pid))

(defun exit (pid)
  (identity pid)
  (error "Not yet implemented."))

(defun monitor ()
  (error "Not yet implemented."))


;;These are pretty high level unit tests that aren't really specific
;;to this file (maybe they should move at some point?
;;
(fiveam:in-suite erlang-in-lisp::eil-suite)
(defmacro with-toplevels (toplevel-list &body body)
  "Macro to let us run the same tests for all the specified concurrency strategies."
  `(loop for classname in ,toplevel-list
       do (progn 
	    (toplevel :process-class classname)
	    ,@body
	    (toplevel-exit))))

(defun send-4-msgs ()
  "Receives a pid and then sends that pid four messages."
  (format t "send-4-msgs started~%")
  (receive ((list pid)
	    (progn 
	      (sleep 2) ;;make sure we aren't taking advantage of concurrency happenstance.
	      (format t "send-4-msgs got pid~%")
	      (send pid :message1 1) 
	      (send pid :message2 2)
	      (send pid :message3 3)
	      (send pid :message4 4)
	      (format t "send-4-msgs sent msgs~%")))))


(fiveam:test simple-send-receive-test
  "Test the send/receive in order."
  (with-toplevels '(thread-process unix-process)
    (let ((sender (spawn 'sender #'send-4-msgs)))	  
	  (send sender (self))

	  (receive-after (((list :message1 i) (is (equal i 1)))) 4 ((fail "timeout~%")))
	  (receive-after (((list :message2 i) (is (equal i 2)))) 4 ((fail "timeout~%")))
	  (receive-after (((list :message3 i) (is (equal i 3)))) 4 ((fail "timeout~%")))
	  (receive-after (((list :message4 i) (is (equal i 4)))) 4 ((fail "timeout~%"))))))

(fiveam:test out-of-order-send-receive-test
  "Test the send/receive with out of order messages."
  (with-toplevels '(thread-process unix-process)
    (let ((sender (spawn 'sender #'send-4-msgs)))
	  
	  (send sender (self))
	  
	  ;;(sleep 2)

	  (receive-after (((list :message2 i) (is (equal i 2)))) 4 ((fail "timeout~%")))
	  (receive-after (((list :message3 i) (is (equal i 3)))) 4 ((fail "timeout~%")))
	  (receive-after (((list :message1 i) (is (equal i 1)))) 4 ((fail "timeout~%")))
	  (receive-after (((list :message4 i) (is (equal i 4)))) 4 ((fail "timeout~%"))))))

(defun non-echo-server ()
  (receive (_ (format t "non-echo-server: got a message"))))

(fiveam:test register-test 
  (with-toplevels '(thread-process unix-process)
    (let ((echo (spawn 'echo #'non-echo-server)))
      (register 'erlang-in-lisp::echo echo)
    
      ;;test unregister, registered, etc.
      (is (process-equal-p (whereis 'echo) echo))
	      
      (send echo :done))))

;;(fiveam:test link-test 
;;  (with-toplevels '(thread-process unix-process)
;;    (let ((fail-pid (spawn 'fail #'(lambda ()
;;				     (error "something went wrong")))))

;;(fiveam:test self-test 
;;  (with-toplevels '(unix-process)
;;    (let ((myself (self)))
;;      (spawn 'send-to-old-self #'(lambda ()
;;				   (print myself)
;;				   (send myself :ack)))
;;
;;      (receive-after (((list :ack) (is (eql t t)));;success
;;		      (_ (fail "did not receive an ack")))
;;		     1
;;		     ((print "timeout") (fail "timeout."))))))

	     
