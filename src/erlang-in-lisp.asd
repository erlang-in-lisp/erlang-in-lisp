;;;-*- Lisp -*-
(in-package :cl-user)

;;this is just for testing purposes...a minor annoyance of mine
;;it makes sure stdout from different threads is redirected to the screen in emacs/slime
;;#+sbcl(setf swank:*globally-redirect-io* t)
#+sbcl(defparameter *stdout* *standard-output*)


;;Add the ../deps directory to the asdf search list:
(pushnew (first (directory #P"../deps/")) asdf:*central-registry* :test 'equal)

(asdf:defsystem :erlang-in-lisp
  :name "Erlang In Lisp"
  :depends-on (:cffi :cffi-grovel :alexandria 
	       :iolib :fare-matcher 
	       :bordeaux-threads ;;:cl-store
	       :fiveam) ;;remove fiveam and should still compile
  :components ((:file "package")
	       (:file "util" :depends-on ("package"))
	       ;;(cffi-grovel:grovel-file "ei-grovel" :depends-on ("package"))
	       (:file "fork" :depends-on ("package"))
	       (:file "process" :depends-on ("util"))
	       (:file "core-eil" :depends-on ("process"))
	       (:file "eil" :depends-on ("core-eil"))
	       (:file "process-unix" :depends-on ("eil" "fork"))
	       (:file "process-threads" :depends-on ("eil"))
	       (:file "eil-pmd" :depends-on ("eil"))
	       (:file "examples" :depends-on ("eil"))))

