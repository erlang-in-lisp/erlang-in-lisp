;;;-*- Lisp -*-

(in-package #:cl-user)

(cl:defpackage #:erlang-in-lisp
  (:use #:cl #:cffi #:alexandria #:fiveam)
  (:export #:spawn #:send #:receive
	   #:toplevel #:toplevel-exit))

(in-package #:erlang-in-lisp)
(fiveam:def-suite eil-suite :description "The erlang-in-lisp test suite.")
