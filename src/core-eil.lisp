(in-package #:erlang-in-lisp)

;;This file contains the thin macro layet that is responsible for
;;calling the the generic functions in process.lisp.  

(defun send (pid &rest msg) 
  (apply #'send* *current-process* pid msg))

(defmacro receive (&body body)
  `(receive-after (,@body) nil ((error "This should not happen."))))

(defun do-receive-after (match-thunk timeout after-thunk)
  (receive* (self) match-thunk timeout after-thunk))

;;this needs to have fancier syntax...
(defmacro receive-after ((&body receive-body) timeout (&body after-body))
  `(do-receive-after
    (receive-code->func ,@receive-body)
    ,timeout
    #'(lambda () ,@after-body)))

(defmacro receive-code->func (&body body)
  "Converts the code specified in the body
to a function taking one argument, a message.

This function returns nil when no match is found
and a closure of no arguments that contains the code to be executed
for the match."
  (let ((msg (gensym)))
     `#'(lambda (,msg)
	 (values
	  (fare-matcher:match ,msg
			      ,@(loop for line in body
				   collect (list (first line) 
						 `#'(lambda ()
						      ,@(rest line)))))
	  ,msg))))

(defun spawn (name func)
  (spawn* *current-process* name func))

(defun spawn-remote (node name func)
  (send node :spawn (self) name func))

(defun self () (self* *current-process*))

(defun node ()
  "Return the node (usually hostname) of this image."
  (error "Not yet implemented."))

;;Starting EIL from the REPL:
(defun toplevel (&key (process-class 'unix-process) (name nil) (sname nil) cookie)
  "Called to enable eil functions such as send and receive to work from the REPL."
  (let ((local-address nil))
    (cond ((and name sname) ;;specified sname and name, errro
	   (error "Specify either name or sname, not both."))
	  (sname ;;only look at hosts file
	   (error "sname functionality not yet implemented"))
	  (cookie 
	   (error "setting cookie not yet implemented.  no security here."))
	  (name ;;look up hostname, get an address we can bind to
	   (setf local-address (iolib:ensure-hostname name)))
	(t;;(and (null sname) (null name))
	 (setf local-address (iolib:make-address #(127 0 0 1)))))
    (setf *current-process* (make-instance (find-class process-class) :name 'eil-repl))
    (toplevel* *current-process* local-address)))

(defun toplevel-exit ()
  (toplevel-exit* *current-process*)
  (setf *current-process* nil))

