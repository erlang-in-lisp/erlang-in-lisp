;;ping pong example, adapted from the cl-muproc manual
(in-package #:erlang-in-lisp)

(defun simple-test ()
  (let ((child (spawn 'simple-test
		      #'(lambda ()
			  (receive
			    ((list :hello1)
			     (format t "hello1~%"))
			    ((list :hello2)
			     (format t "hello2~%")))
			  (receive
			    ((list :hello1)
			     (format t "hello1~%"))
			    ((list :hello2)
			     (format t "hello2~%")))
			  (format t "exiting~%")))))
    (send child :hello1)
    (send child :hello2)
    (format t "finished send~%")
    (wait)))
    ;;(send child :hello2)))

;;ping pong test follows (from the cl-muproc examples):
(defun ping (pong count)
  (loop for iter from 1 to count
     do (send pong :ping iter (self))
     do (receive
  	  ((list :pong) (format t "PING -- Got pong.~%"))))
  (send pong :done))


(defun pong ()
  (receive
    ((list :ping iter ping)
     (format t "PONG -- Got ping.~%")
     (send ping :pong)
     (format t "PONG -- Sent pong.~%")
     (pong)) ;;check that this tail call can be eliminated.
    ((list :done)
     (format t "PONG -- Got done."))))

(defun ping-pong (count)
  (let* ((pong (spawn 'pong #'pong)))
	;;(ping (spawn 'ping #'(l))))
    (ping pong count)))
    ;;(values ping pong)))
;;to test do something like: (ping-pong 3)

;;(erlang-in-lisp::make-pid 'pong (iolib:make-address #(192 168 1 2)) 45 nil)
(defun dist-ping-pong (count pong-location)
  (ping pong-location count))

;;Calculate PI example
;;this is taken from the 'Using MPI' book
(defun f (x h answer-pid)
  "Estimate a portion of the area under the curve 4/(1+x^2) from x to x+h."
  (let* ((dx (+ (/ h 2) x))
	 (ans (* (/ 4.0 (+ 1.0 (* dx dx))) h)))
    (send answer-pid :little-sum ans)))

(defun calculate-pi (num-intervals)
  "Calculate pi.  The calculation is done by evaluating the integral of
1/(1+x^2) from 0 to 1 and multiplying by 4.  num-intervals is the
number of squares used to estimate the integral.  This particular
approach is extremely fine grained (each process calculates the area
of one square), and thus it is inefficient."
  (let ((h (/ 1.0 num-intervals))
	 (my-pi 0))
    (identity h)
    (loop for x from 0 to num-intervals
       do (spawn 'calc-pi #(lambda () (funcall #'f (* x h) h (self)))))
    (loop for x from 0 to num-intervals
       do (receive
	    ((list :little-sum little-sum)
	     (incf my-pi little-sum))))
    (format t "pi is: ~A~%" my-pi)))



