(in-package #:erlang-in-lisp)

(defclass testing () 
  ((p1 :initform 1)
   (p2 :initform 2)))

(defun myreceive (socket)
  (iolib:with-accept-connection (connection socket :timeout 4)
    (if (null connection) ;;timeout, no connection
	(error "timeout")
	(list (cl-store:restore connection)
	      (cl-store:restore connection)
	      (cl-store:restore connection)))))

(defun mysend ()
  (iolib:with-open-socket (socket :ipv6 nil) ;;cache these connections?
    (iolib:connect socket 
		   (iolib:make-address #(127 0 0 1))
		   :port 8890)
    (cl-store:store 'hello-world socket)
    (cl-store:store (make-instance 'testing) socket)
    (cl-store:store 2 socket)
    (finish-output socket)))

(defun mytests ()
  (let* ((socket (iolib:make-socket :ipv6 nil
				   :connect :passive
					    :local-host #(127 0 0 1)
					    :local-port 8890))
	 (thread (bordeaux-threads:make-thread #'(lambda () (myreceive socket)))))
    (mysend)
    (bordeaux-threads:join-thread thread)
    (close socket)
    thread))