;;;-*- Lisp -*-

(in-package :cl-user)

(pushnew (first (directory #P"../deps/")) asdf:*central-registry* :test 'equal)
(pushnew (first (directory #P"../src/")) asdf:*central-registry* :test 'equal)

(asdf:defsystem :erlang-in-lisp-test
  :name "Erlang In Lisp -- Test"
  :depends-on (:fiveam)
  :components ((:file "package")
	       (:file "test-test" :depends-on ("package"))))



