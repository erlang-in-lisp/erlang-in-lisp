;;; -*- Mode: Lisp ; Base: 10 ; Syntax: ANSI-Common-Lisp -*-

(in-package #:cl-user)

(cl:defpackage #:erlang-in-lisp-test
  (:use #:cl #:fiveam)
  (:export #:first-test))

