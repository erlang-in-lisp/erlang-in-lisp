;;this is a test of the testing system ;)

(in-package #:erlang-in-lisp-test)

(test add-func
 "Test the add function"
 (is (= 2 (+ 2 0)))
 (is (= 3 (+ 2 -2))))
